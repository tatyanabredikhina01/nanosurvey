﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NanoSurvey.Migrations
{
    public partial class ChangeResultForeignKeys : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Results_Answers_AnswerId",
                table: "Results");

            migrationBuilder.DropForeignKey(
                name: "FK_Results_Questions_QuestionId",
                table: "Results");

            migrationBuilder.AlterColumn<int>(
                name: "QuestionId",
                table: "Results",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "AnswerId",
                table: "Results",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Results_Answers_AnswerId",
                table: "Results",
                column: "AnswerId",
                principalTable: "Answers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Results_Questions_QuestionId",
                table: "Results",
                column: "QuestionId",
                principalTable: "Questions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Results_Answers_AnswerId",
                table: "Results");

            migrationBuilder.DropForeignKey(
                name: "FK_Results_Questions_QuestionId",
                table: "Results");

            migrationBuilder.AlterColumn<int>(
                name: "QuestionId",
                table: "Results",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<int>(
                name: "AnswerId",
                table: "Results",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddForeignKey(
                name: "FK_Results_Answers_AnswerId",
                table: "Results",
                column: "AnswerId",
                principalTable: "Answers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Results_Questions_QuestionId",
                table: "Results",
                column: "QuestionId",
                principalTable: "Questions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
