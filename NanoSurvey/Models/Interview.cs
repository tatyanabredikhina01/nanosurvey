﻿using System;
using System.Collections.Generic;

namespace NanoSurvey.Models
{
    public class Interview
    {
        public int Id { get; set; }
        public DateTime CreationDate { get; set; }
        public string UserInfo  { get; set; }
        public int SurveyId { get; set; }

        public virtual Survey Survey { get; set; }
        public virtual ICollection<Result> Results { get; set; }
    }
}