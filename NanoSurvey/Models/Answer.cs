﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace NanoSurvey.Models
{
    public class Answer
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public int QuestionId { get; set; }
        public virtual Question Question { get; set; }
        public virtual ICollection<Result> Results { get; set; }

    }
}