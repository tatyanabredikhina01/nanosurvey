﻿using System.Collections.Generic;

namespace NanoSurvey.Models
{
    public class Survey
    {
        public int Id { get; set; }
        public string Title { get; set; }
        
        public virtual ICollection<Question> Questions { get; set; }
        public virtual ICollection<Interview> Interview { get; set; }
        
    }
}