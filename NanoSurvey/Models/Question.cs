﻿using System.Collections.Generic;

namespace NanoSurvey.Models
{
    public class Question
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public int SortNumber { get; set; }
        public int SurveyId { get; set; }
        
        public virtual Survey Survey { get; set; }
        public virtual ICollection<Answer> Answers { get; set; }
        public virtual ICollection<Result> Results { get; set; }

    }
}