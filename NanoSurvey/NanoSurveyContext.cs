﻿using Microsoft.EntityFrameworkCore;
using NanoSurvey.Models;

namespace NanoSurvey
{
    public class NanoSurveyContext : DbContext
    {
        public DbSet<Answer> Answers { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Survey> Surveys { get; set; }
        public DbSet<Interview> Interviews { get; set; }
        public DbSet<Result> Results { get; set; }

        public NanoSurveyContext(DbContextOptions options) : base(options)
        {
            // Database.Migrate();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Question>().HasIndex(q => new {q.SortNumber, q.SurveyId}).IsUnique();
        }
    }
}