﻿using System.Collections.Generic;

namespace NanoSurvey.Dto
{
    public class QuestionViewDto
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public IEnumerable<AnswerViewDto> Answers { get; set; }
    }
}