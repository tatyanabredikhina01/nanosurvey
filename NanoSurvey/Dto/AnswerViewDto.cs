﻿namespace NanoSurvey.Dto
{
    public class AnswerViewDto
    {
        public int Id { get; set; }
        public string Text { get; set; }
    }
}