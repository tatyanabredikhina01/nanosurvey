﻿namespace NanoSurvey.Dto
{
    public class ResultDto
    {
        public int AnswerId { get; set; }
        public int QuestionId { get; set; }
        public int InterviewId { get; set; }
    }
}