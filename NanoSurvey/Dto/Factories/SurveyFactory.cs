﻿using AutoMapper;
using NanoSurvey.Models;

namespace NanoSurvey.Dto.Factories
{
    public class SurveyFactory
    {
        private readonly IMapper _mapper;

        public class MappingProfile : Profile
        {
            public MappingProfile()
            {
                CreateMap<Answer,AnswerViewDto>();
                CreateMap<Question,QuestionViewDto>();
                CreateMap<ResultDto,Result>();
            }
        }

        public SurveyFactory(IMapper mapper)
        {
            _mapper = mapper;
        }

        public QuestionViewDto BuildQuestionViewDto(Question entity)
        {
            return _mapper.Map<QuestionViewDto>(entity);
        }

        public Result FromDto(ResultDto dto)
        {
            return _mapper.Map<Result>(dto);
        }
        
    }
}