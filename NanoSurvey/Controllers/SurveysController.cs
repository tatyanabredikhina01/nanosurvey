﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NanoSurvey.Dto;
using NanoSurvey.Dto.Factories;
using NanoSurvey.Infrastructure;

namespace NanoSurvey.Controllers
{
    [ApiController]
    public class SurveysController : ControllerBase
    {
        private readonly SurveyService _service;
        private readonly SurveyFactory _factory;

        public SurveysController(
            SurveyService service,
            SurveyFactory factory)
        {
            _service = service;
            _factory = factory;
        }

        [HttpGet]
        [Route("/questions/{id}")]
        public async Task<QuestionViewDto> GetQuestionById(int id)
        {
            var question = await _service.GetQuestionByIdAsync(id);
            if (question == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            
            return _factory.BuildQuestionViewDto(question);
        }

        [HttpPost]
        [Route("/interview")]
        public async Task<int> SaveResult(ResultDto dto)
        {
            var result = _factory.FromDto(dto);
            return await _service.SaveResultAsync(result);
        }
    }
}