﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NanoSurvey.Models;

namespace NanoSurvey.Infrastructure
{
    public class Repository : IRepository
    {
        private readonly NanoSurveyContext _context;

        public Repository(NanoSurveyContext context)
        {
            _context = context;
        }

        public async Task<Question> GetQuestionByIdAsync(int id)
        {
            return await _context.Questions.FirstOrDefaultAsync(q => q.Id == id);
        }

        public async Task<int> GetQuestionIdBySurveyIdAndSortNumberAsync(int sortNumber, int surveyId)
        {
            return await _context.Questions
                .Where(q => q.SurveyId == surveyId && q.SortNumber == sortNumber)
                .Select(q => q.Id)
                .FirstOrDefaultAsync(); 
        }

        public async Task SaveResultAsync(Result result)
        {
            await _context.Results.AddAsync(result);
            await _context.SaveChangesAsync();
        }

        private bool _disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }

            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}