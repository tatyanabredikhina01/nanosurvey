﻿using Microsoft.Extensions.DependencyInjection;
using NanoSurvey.Dto.Factories;

namespace NanoSurvey.Infrastructure
{
    public static class ServiceLocator
    {
        public static void RegisterServices(this IServiceCollection services)
        {
            services.AddTransient<IRepository, Repository>();
            services.AddTransient<SurveyService>();
            services.AddSingleton<SurveyFactory>();
        }
    }
}