﻿using System;
using System.Threading.Tasks;
using NanoSurvey.Models;

namespace NanoSurvey.Infrastructure
{
    public interface IRepository : IDisposable
    {
        Task<Question> GetQuestionByIdAsync(int id);
        Task<int> GetQuestionIdBySurveyIdAndSortNumberAsync(int sortNumber, int surveyId);
        Task SaveResultAsync(Result result);
    }
}