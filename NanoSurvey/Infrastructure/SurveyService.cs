using System.Threading.Tasks;
using NanoSurvey.Models;

namespace NanoSurvey.Infrastructure
{
    public class SurveyService
    {
        private readonly IRepository _repository;

        public SurveyService(IRepository repository)
        {
            _repository = repository;
        }

        public async Task<Question> GetQuestionByIdAsync(int id)
        {
            return await _repository.GetQuestionByIdAsync(id);
        }

        public async Task<int> SaveResultAsync(Result result)
        {
            await _repository.SaveResultAsync(result);
            var nextQuestionId = await GetNextQuestionIdAsync(result.QuestionId);
            return nextQuestionId;
        }

        private async Task<int> GetNextQuestionIdAsync(int currentQuestionId)
        {
            var question = await _repository.GetQuestionByIdAsync(currentQuestionId);
            return await _repository
                .GetQuestionIdBySurveyIdAndSortNumberAsync(
                    question.SortNumber + 1,
                    question.SurveyId);
        }
    }
}
