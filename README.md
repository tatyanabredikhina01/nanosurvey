## Запуск через docker-compose
Hеобходимо в директории NanoSurvey запустить команду `docker-compose up` .
При этом произойдет следующее:
- создастся база из переменной окружения POSTGRES_DB в NanoSurvey/docker-compose.yml
- выполнится скрипт, который лежит в NanoSurvey/scripts/NanoSurveyDatabaseScript.sql . Этот скрипт создаст необходимые таблицы и заполнит их шаблонными данными.
- скачается образ и запустится контейнер с postgresql.
- соберется образ приложения из NanoSurvey/Dockerfile и запустится контейнер с ним. Приложение будет доступно по адресу http://localhost:9001.

## Запуск локально 
1.  поменять `Host=postgres` на `Host=localhost` в DefaultConnection в NanoSurvey/appsetings.json
2.  раскомментировать `Database.Migrate()` в NanoSurvey/NanoSurveyContext.cs, чтобы при инициализации контекста к базе применились миграции.
3. заполнить базу (вручную или написав скрипт).
4. выполнить команду `dotnet run`
5. приложение будет доступно по адресу http://localhost:5000 и https://localhost:5001.
